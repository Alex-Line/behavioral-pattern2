package com.iteco.alexline.state;

import com.iteco.alexline.state.state.IState;

public interface IManifold {

    void enterMoney(final Double money) throws Exception;

    void chooseDevice(final String device) throws Exception;

    void chooseFile(final String file) throws Exception;

    void printFile() throws Exception;

    void takeMoneyBack() throws Exception;

    IState getState();

    void setState(final IState state);

    Double getCredentialAmount();

    void setCredentialAmount(final Double amount);

    Double getCost();

}