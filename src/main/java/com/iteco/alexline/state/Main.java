package com.iteco.alexline.state;

public class Main {

    public static void main(String[] args) throws Exception {
        //happy path
        IManifold manifold = new Manifold();
        manifold.enterMoney(12.56);
        manifold.chooseDevice("wi-fi");
        manifold.chooseFile("some file");
        manifold.printFile();
        manifold.takeMoneyBack();

        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - -");

        //several printing
        IManifold manifold2 = new Manifold();
        manifold2.enterMoney(2.0);
        manifold2.chooseDevice("wi-fi");
        manifold2.chooseFile("some file");
        manifold2.printFile();
        manifold2.chooseDevice("USB");
        manifold2.chooseFile("other file");
        manifold2.printFile();
        manifold2.takeMoneyBack();

        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - -");

        //no enough money
        IManifold manifold3 = new Manifold();
        manifold3.enterMoney(0.5);
        manifold3.chooseDevice("USB");
        manifold3.chooseFile("other file");
        try {
            manifold3.printFile();
        } catch (Exception e) {
            System.out.println(e);
        }
        manifold3.takeMoneyBack();

        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - -");

        //enter money after cash back
        IManifold manifold4 = new Manifold();
        manifold4.enterMoney(100.500);
        manifold4.chooseDevice("WI-FI");
        manifold4.chooseFile("some file");
        manifold4.printFile();
        manifold4.takeMoneyBack();
        try {
            manifold4.enterMoney(100.500);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}