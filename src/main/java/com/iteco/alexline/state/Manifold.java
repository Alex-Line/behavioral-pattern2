package com.iteco.alexline.state;

import com.iteco.alexline.state.state.IState;
import com.iteco.alexline.state.state.InitialState;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Manifold implements IManifold {

    private Double credentialAmount = 0.0;
    private Double cost = 1.0;
    private IState state;

    public Manifold() {
        this.state = new InitialState();
    }

    @Override
    public void enterMoney(final Double money) throws Exception {
        state.enterMoney(this, money);
    }

    @Override
    public void chooseDevice(final String device) throws Exception {
        this.state.chooseDevice(this, device);
    }

    @Override
    public void chooseFile(final String file) throws Exception {
        this.state.chooseFile(this, file);
    }

    @Override
    public void printFile() throws Exception {
        this.state.printFile(this);
    }

    @Override
    public void takeMoneyBack() throws Exception {
        this.state.takeMoneyBack(this, credentialAmount);
    }

}