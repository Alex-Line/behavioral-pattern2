package com.iteco.alexline.state.state;

import com.iteco.alexline.state.IManifold;

public abstract class BaseState implements IState {

    @Override
    public void enterMoney(final IManifold manifold, final Double money) throws Exception {
        if (money > 0) System.out.println(String.format("Было внесено %s рублей", money));
        else throw new Exception("Введено некорректное количество денег! Попробуйте снова");
        manifold.setState(new ReadyToChooseDeviceState());
        manifold.setCredentialAmount(manifold.getCredentialAmount() + money);
    }

    @Override
    public void chooseDevice(final IManifold manifold, final String device) throws Exception {
        throw new Exception("Невозможно выбрать устройство до того как будут внесены деньги");
    }

    @Override
    public void chooseFile(final IManifold manifold, final String file) throws Exception {
        throw new Exception("Невозможно выбрать файл до того как будет выбрано устройство");
    }

    @Override
    public void printFile(final IManifold manifold) throws Exception {
        throw new Exception("Невозможно распечатать файл до того как будут внесены деньги");
    }

    @Override
    public void takeMoneyBack(final IManifold manifold, final Double rest) throws Exception {
        if (manifold.getCredentialAmount() >= 0) System.out.println(String.format("Возвращено %s рублей",
                manifold.getCredentialAmount()));
        else throw new Exception("Невозможно вернуть сдачу до того как будут внесены деньги");
        manifold.setState(new NoMoreMoneyState());
    }

}