package com.iteco.alexline.state.state;

import com.iteco.alexline.state.IManifold;

public interface IState {

    void enterMoney(final IManifold manifold, final Double money) throws Exception;

    void chooseDevice(final IManifold manifold, final String device) throws Exception;

    void chooseFile(final IManifold manifold, final String file) throws Exception;

    void printFile(final IManifold manifold) throws Exception;

    void takeMoneyBack(final IManifold manifold, final Double rest) throws Exception;

}