package com.iteco.alexline.state.state;

import com.iteco.alexline.state.IManifold;

public class NoMoreMoneyState extends BaseState {

    @Override
    public void enterMoney(IManifold manifold, Double money) throws Exception {
        throw new Exception("Все деньги были возвращены или использованы. Пожалуйста, инициируйте новый сеанс работы с устройством");
    }

    @Override
    public void takeMoneyBack(IManifold manifold, Double rest) throws Exception {
        throw new Exception("Все деньги были возвращены или использованы.");
    }

}