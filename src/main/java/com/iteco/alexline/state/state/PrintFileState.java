package com.iteco.alexline.state.state;

import com.iteco.alexline.state.IManifold;

public class PrintFileState extends BaseState {

    @Override
    public void printFile(final IManifold manifold) throws Exception {
        if (manifold.getCredentialAmount() < manifold.getCost())
            throw new Exception("Внесено недостаточно денег для выполнения операции");
        manifold.setCredentialAmount(manifold.getCredentialAmount() - manifold.getCost());
        System.out.println("Печатаем содержимое файла...");
        manifold.setState(new ReadyToChooseDeviceState());
    }

}