package com.iteco.alexline.state.state;

import com.iteco.alexline.state.IManifold;

public class ReadyToChooseDeviceState extends BaseState {

    @Override
    public void chooseDevice(final IManifold manifold, final String device) {
        System.out.println(String.format("Было выбрано устройство %s", device));
        manifold.setState(new ReadyToChooseFileState());
    }
}