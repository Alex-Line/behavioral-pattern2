package com.iteco.alexline.state.state;

import com.iteco.alexline.state.IManifold;

public class ReadyToChooseFileState extends BaseState {

    @Override
    public void chooseDevice(final IManifold manifold, final String device) throws Exception {
        System.out.println(String.format("Было выбрано устройство %s", device));
        manifold.setState(new ReadyToChooseFileState());
    }

    @Override
    public void chooseFile(final IManifold manifold, final String file) throws Exception {
        System.out.println(String.format("Был выбран файл %s", file));
        manifold.setState(new PrintFileState());
    }

}